package com.example.demo.controllers;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.example.demo.models.Tarea;
import com.example.demo.services.TareaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TareaController {
    Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    TareaService servicioDeTarea;

    @GetMapping(path = "/listarTarea")
    public List<Tarea> metodoTarea() {
        return servicioDeTarea.listarTarea();
    }

    // Registro
    @PostMapping(path = "/registroTarea")
    public Tarea postTarea(@RequestBody Tarea request) {

        log.info("Peticion recibida {}", request);
        return servicioDeTarea.guardarTarea(request);
    }

    // Actualizar
    @PutMapping(path = "/actualizarTarea/{id}")
    public Tarea actualizarTarea(@PathVariable(value = "id") Long tareaId, @RequestBody Tarea request) {

        log.info("Peticion recibida {}", request);
        return servicioDeTarea.actualizarTarea(tareaId, request);
    }
    
}
