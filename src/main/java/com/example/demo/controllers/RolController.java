package com.example.demo.controllers;

import com.example.demo.services.RolService;
import com.example.demo.models.Rol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.Logger;

import java.util.List;
import org.apache.logging.log4j.LogManager;

@RestController
public class RolController {
    
    Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    RolService servicioDeRol;
    @GetMapping(path = "/listarRol")

    public List<Rol> metodoRol(){
        return servicioDeRol.listarRol();
    }

    @PostMapping(path = "/registroRol")
    public Rol registroRol(@RequestBody Rol request){
        
        log.info("Peticion recibida {}", request);
        return servicioDeRol.registrarRol(request);
    }
}
