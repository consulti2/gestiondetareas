package com.example.demo.controllers;

import java.util.List;

import com.example.demo.models.Historial;
import com.example.demo.services.HistorialService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HistorialController {
    Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    HistorialService servicioDeHistorial;

    @GetMapping(path = "/listarHistorial")
    public List<Historial> metodoHistorial(){
        return servicioDeHistorial.listarHistorial();
    }

     // Registro
     @PostMapping(path = "/registroHist")
     public Historial guardarHistorial(@RequestBody Historial request) {
 
         log.info("Peticion recibida {}", request);
         return servicioDeHistorial.guardarHistorial(request);
     }
}
