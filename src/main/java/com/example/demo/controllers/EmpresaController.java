package com.example.demo.controllers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.example.demo.models.Empresa;
import com.example.demo.services.EmpresaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmpresaController {
    
    Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    EmpresaService servicioDeEmpresa;
    @GetMapping(path = "/listarEmpresa")

    public List<Empresa> metodoEmpresa(){
        return servicioDeEmpresa.listarEmpresa();
    }

    @PostMapping(path = "/registrarEmpresa")
    public Empresa registroEmpresa(@RequestBody Empresa request){
        
        log.info("Peticion recibida {}", request);
        return servicioDeEmpresa.registrarEmpresa(request);
    }

}
