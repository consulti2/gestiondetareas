package com.example.demo.repository;

import java.util.Optional;

import com.example.demo.models.Tarea;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TareaRepository extends JpaRepository<Tarea, Long> {

    public boolean existsByIdAndEstado(String string, String estado);

    public Optional<Tarea> findByEstado(String estado);
    public boolean existsByEstado(String estado);

}
