package com.example.demo.services;

import java.util.List;
import com.example.demo.models.Tarea;
import org.springframework.stereotype.Service;

@Service
public interface TareaService {
    public List<Tarea> listarTarea();

    public Tarea guardarTarea(Tarea request);

    public Tarea actualizarTarea(Long tareaId, Tarea request);

}