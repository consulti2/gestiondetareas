package com.example.demo.services;

import java.util.List;
import com.example.demo.models.Historial;
import org.springframework.stereotype.Service;

@Service
public interface HistorialService {
    public List<Historial> listarHistorial();

    public Historial guardarHistorial(Historial request);
    
}
