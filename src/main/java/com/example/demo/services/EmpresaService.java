package com.example.demo.services;

import org.springframework.stereotype.Service;

import java.util.List;
import com.example.demo.models.Empresa;

@Service
public interface EmpresaService {
   
    public List<Empresa> listarEmpresa();
    public Empresa registrarEmpresa(Empresa request);
}
