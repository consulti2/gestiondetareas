package com.example.demo.services;

import org.springframework.stereotype.Service;

import java.util.List;
import com.example.demo.models.Rol;

@Service
public interface RolService {
   
    public List<Rol> listarRol();
    public Rol registrarRol(Rol request);
}
