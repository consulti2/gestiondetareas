package com.example.demo.services.impl;

import java.util.Date;
import java.util.List;

import com.example.demo.models.Historial;
import com.example.demo.repository.HistorialRepository;
import com.example.demo.services.HistorialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

@Service
public class HistorialServiceImpl implements HistorialService {

    @Autowired
    HistorialRepository repositorioHistorial;

    // Listar
    public List<Historial> listarHistorial() {

        return repositorioHistorial.findAll();
    }

    public Historial guardarHistorial(Historial request) throws ResourceAccessException {
        Historial response = new Historial();
        // String estado_nuevo;
        try {
            request.setFecha_de_registro(new Date());
            request.setDescripcion("Tarea actualizada a: ");
            response = repositorioHistorial.save(request);

        } catch (Exception e) {
            throw new ResourceAccessException(e.getMessage());
        }
        return response;

    }
}
