package com.example.demo.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.demo.models.Historial;
import com.example.demo.models.Tarea;
import com.example.demo.repository.TareaRepository;
import com.example.demo.services.TareaService;
import com.example.demo.services.HistorialService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

@Service
public class TareaServiceImpl implements TareaService {
    @Autowired
    TareaRepository repositorioTarea;

    // Listar
    public List<Tarea> listarTarea() {

        return repositorioTarea.findAll();
    }

    // registrar
    public Tarea guardarTarea(Tarea request) throws ResourceAccessException {
        Tarea response = new Tarea();

        try {
            request.setEstado("registrada");
            request.setFecha_de_registro(new Date());
            response = repositorioTarea.save(request);
        } catch (Exception e) {
            throw new ResourceAccessException(e.getMessage());
        }

        return response;
    }

    // actualización
    public Tarea actualizarTarea(Long tareaId, Tarea request) throws ResourceAccessException {
        Tarea response = new Tarea();

        Historial objectodehistorial = new Historial();

        Optional<Tarea> optionalTarea = repositorioTarea.findById(tareaId);
        Tarea tarea = optionalTarea.orElse(request);
        try {
            if (!repositorioTarea.existsById(tareaId)) {
                throw new ResourceAccessException("Tarea no encontrada");
            }
            
            // si la tarea esta cancelada
            Optional<Tarea> optionalTareaEstado = repositorioTarea.findByEstado(request.getEstado());
            String estadoCancelada = "cancelada";
            if (optionalTareaEstado.isPresent()) {
                if (request.getEstado().equals(estadoCancelada.getEstado())) {
                    throw new ResourceAccessException("Tarea no se puede modificar, tarea cancelada");
                }
            }

            // si la fecha fin de la tarea es menor a la fecha actual
            Date fechaActual = new Date();
            if (!(request.getEstado() == "finalizada") && (request.getFecha_fin() == fechaActual)) {
                throw new ResourceAccessException("Tarea menor a la fecha actual, no se puede modificar");
            }

            tarea.setId(request.getId() != null ? request.getId() : tarea.getId());
            tarea.setNombre(request.getNombre() != null ? request.getNombre() : tarea.getNombre());
            tarea.setEstado(request.getEstado() != null ? request.getEstado() : tarea.getEstado());
            tarea.setUsuario_id(request.getUsuario_id() != null ? request.getUsuario_id() : tarea.getUsuario_id());

            request.setFecha_de_registro(new Date());
            response = repositorioTarea.save(tarea);

            objectodehistorial.setDescripcion("Tarea actualizada a " + request.getEstado());
            // HistorialService.guardarHistorial(objectodehistorial);

        } catch (Exception e) {
            throw new ResourceAccessException(e.getMessage());
        }
        return response;
    }
}
