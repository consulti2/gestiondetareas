package com.example.demo.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import com.example.demo.models.Empresa;
import com.example.demo.repository.EmpresaRepository;
import com.example.demo.services.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService{

    @Autowired
    EmpresaRepository repositorioEmpresa;

    public List<Empresa> listarEmpresa(){
        return repositorioEmpresa.findAll();
    }

    
    public Empresa registrarEmpresa(Empresa request){
        return repositorioEmpresa.save(request);
    }
}
