package com.example.demo.services.impl;

import com.example.demo.models.Rol;
import com.example.demo.repository.RolRepository;
import com.example.demo.services.RolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolServiceImpl implements RolService {

    @Autowired
    RolRepository repositorioRol;

    public List<Rol> listarRol(){
        return repositorioRol.findAll();
    }

    public Rol registrarRol(Rol request){
        return repositorioRol.save(request);
    }
    
}
