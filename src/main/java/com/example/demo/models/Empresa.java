package com.example.demo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "empresa")
public class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre", length = 200, nullable = false)
    private String nombre;
   
    // @ManyToMany
    // @JoinTable(
    //     name = "empresa_roles",
    //     joinColumns = @JoinColumn(name = "empresa_id"),
    //     inverseJoinColumns = @JoinColumn(name ="rol_id")
    // )
    // List<Rol> roles;
}